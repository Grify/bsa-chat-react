const months = new Map();
months.set(1, "January");
months.set(2, "February");
months.set(3, "March");
months.set(4, "April");
months.set(5, "May");
months.set(6, "June");
months.set(7, "July");
months.set(8, "August");
months.set(9, "September");
months.set(10, "October");
months.set(11, "November");
months.set(12, "December");
const daysOfWeek = new Map();
daysOfWeek.set(0, "Sunday");
daysOfWeek.set(1, "Monday");
daysOfWeek.set(2, "Tuesday");
daysOfWeek.set(3, "Wednesday");
daysOfWeek.set(4, "Thursday");
daysOfWeek.set(5, "Friday");
daysOfWeek.set(6, "Saturday");


const ISOtoDateAndTime = (ISOTime) => {
    const date = new Date(ISOTime);
    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let dt = date.getDate();
    let hours = date.getHours();
    let minutes = date.getMinutes();

    if (dt < 10) {
        dt = '0' + dt;
    }
    if (month < 10) {
        month = '0' + month;
    }
    if (hours < 10) {
        hours = '0' + hours;
    }
    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    return dt + '.' + month + '.' + year + ' ' + hours + ':' + minutes;
};
const ISOtoDate = (ISOTime) => {
    const date = new Date(ISOTime);
    let month = date.getMonth() + 1;
    let dt = date.getDate();
    let dayOfWeek = date.getDay();

    let days = Math.round((Date.parse(new Date()) - Date.parse(date)) / 86400000);

    if (days === 0) {
        return "Today";
    }
    if (days === 1) {
        return "Yesterday";
    }

    return daysOfWeek.get(dayOfWeek) + ', ' + dt + ' ' + months.get(month);
};
const ISOtoTime = (ISOTime) => {
    const date = new Date(ISOTime);
    let hours = date.getHours();
    let minutes = date.getMinutes();

    if (hours < 10) {
        hours = '0' + hours;
    }
    if (minutes < 10) {
        minutes = '0' + minutes;
    }

    return hours + ':' + minutes;
};

export {ISOtoDate, ISOtoTime, ISOtoDateAndTime};