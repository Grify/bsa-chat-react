import * as React from 'react';

class HeaderMessagesCount extends React.Component {

    render() {
        return(
            <div>
            <span className={"header-messages-count"}>
                {this.props.messagesCount}
            </span>
                {" "}
                messages
            </div>
        );
    }
}

export default HeaderMessagesCount;