import * as React from 'react';

class HeaderUsersCount extends React.Component {

    render() {
        return(
            <div>
            <span className={"header-users-count"}>
                {this.props.usersCount}
            </span>
                {" "}
                users
            </div>
        );
    }
}

export default HeaderUsersCount;