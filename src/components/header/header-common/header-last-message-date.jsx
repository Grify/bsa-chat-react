import * as React from 'react';
import {ISOtoDateAndTime} from "../../../functions/convert-date";

class HeaderLastMessageDate extends React.Component{

    render() {
        return(
            <div>
                Last message at
                {" "}
                <span className={"header-last-message-date"}>
                    {ISOtoDateAndTime(this.props.lastMessageDate.createdAt)}
                </span>
            </div>
        );
    }
}

export default HeaderLastMessageDate;