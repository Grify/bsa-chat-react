import * as React from 'react';

class HeaderTitle extends React.Component{

    render() {
        return(
            <div className={"header-title"}>
                <h1>My Chat</h1>
            </div>
        );
    }
}

export default HeaderTitle;