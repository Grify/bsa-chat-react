import * as React from 'react';
import HeaderTitle from "./header-common/header-title";
import HeaderUsersCount from "./header-common/header-users-count";
import HeaderMessagesCount from "./header-common/header-messages-count";
import HeaderLastMessageDate from "./header-common/header-last-message-date";


class Header extends React.Component{

    render() {
        return(
            <div className={"header"}>
                <HeaderTitle/>
                <HeaderUsersCount
                    usersCount={this.props.usersCount}
                />
                <HeaderMessagesCount
                    messagesCount={this.props.messagesCount}
                />
                <HeaderLastMessageDate
                    lastMessageDate={this.props.lastMessageDate}
                />
            </div>
        );
    }
}

export default Header;