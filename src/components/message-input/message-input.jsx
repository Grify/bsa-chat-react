import * as React from 'react';
import generateUUID from "../../functions/generate-uuid";

class MessageInput extends React.Component{
    constructor(props) {
        super(props);
        this.state = {body: ""};
        this.handleSendMessage = this.handleSendMessage.bind(this);
    }

    handleSendMessage() {
        if (this.state.body === "") {
            return;
        }
        // Go to the bottom
        setTimeout(() => {
            document.getElementsByClassName("message-list")[0].scrollTop = document.getElementsByClassName("message-list")[0].scrollHeight;
            } , 200);
        // If editing
        const id = document.getElementById("hidden-input-id").value;
        if (id !== "") {
            this.props.onMessageEdit({id: id, text: this.state.body, editedAt: new Date().toISOString()});
            document.getElementById("hidden-input-id").value = "";
            document.getElementsByClassName("message-input-text")[0].value = "";
            document.getElementsByClassName("editPostMarker")[0].style.opacity = 0;
            this.setState({body: ""});
            return;
        }
        this.props.addMessage({id: generateUUID(), text: this.state.body, createdAt: new Date().toISOString()});
        document.getElementsByClassName("message-input-text")[0].value = "";
        this.setState({body: ""});
    };

    render() {
        return(
            <div className={"message-input"}>
                <span className="editPostMarker">
                    You are editing your message.
                </span>
                <div className={"input-form"}>
                    <input id={"hidden-input-id"} type="text" hidden={"hidden"}/>
                    <textarea
                        onChange={event => this.setState({body: event.target.value})}
                        className={"message-input-text"}
                        placeholder={"Message"}
                    />
                    <button onClick={this.handleSendMessage} className={"message-input-button"}>Send</button>
                </div>
            </div>
        );
    }
}

export default MessageInput;