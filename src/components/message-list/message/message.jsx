import * as React from 'react';
import LikeButton from "../../common/like-button";
import {ISOtoTime} from "../../../functions/convert-date";

class Message extends React.Component{
    constructor(props) {
        super(props);
        this.state = {message: props.message};
    }

    render() {
        return(
            <div className={"message"}>
                <div className={"user-info"}>
                    <img src={this.state.message.avatar} alt="User Avatar" className={"message-user-avatar"}/>
                    <span className={"message-user-name"}>{this.state.message.user}</span>
                </div>
                <div className={"message-container"}>
                    <p className={"message-text"}>{this.state.message.text}</p>
                    <span className={"message-time"}>{ISOtoTime(this.state.message.createdAt)}</span>
                    <LikeButton/>
                </div>
            </div>
        );
    }
}

export default Message;