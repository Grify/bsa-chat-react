import * as React from 'react';
import Message from "./message/message";
import OwnMessage from "./own-message/own-message";
import MessageInput from "../message-input/message-input";
import MessageDivider from "../common/messages-divider";
import generateUUID from "../../functions/generate-uuid";

class MessageList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {messages: props.messages};
        this.handleMessageAdd = this.handleMessageAdd.bind(this);
        this.handleMessageDelete = this.handleMessageDelete.bind(this);
        this.handleMessageEdit = this.handleMessageEdit.bind(this);
    }

    handleMessageEdit(messagePayload) {
        const newMessages = this.state.messages.map(
            message => message.id === messagePayload.id ?
                {id: generateUUID(), text: messagePayload.text, createdAt: message.createdAt,
                    editedAt: messagePayload.editedAt} : message);
        this.setState({messages: newMessages});
        this.props.onChangeMessages(newMessages);
    }
    handleMessageDelete(id) {
        const newMessages = this.state.messages.filter(message => message.id !== id);
        this.setState({messages: newMessages});
        this.props.onChangeMessages(newMessages);
    }
    handleMessageAdd(messagePayload) {
        let newMessages = this.state.messages;
        newMessages.push(messagePayload);
        this.setState({messages: newMessages});
        this.props.onChangeMessages(newMessages);
    }

    render() {
        let counter = new Date('1990').toISOString();
        return(
            <div className={"message-list"}>
                    {this.state.messages.map((message, index) => {
                        if (new Date(message.createdAt).getDate() !== new Date(counter).getDate() ||
                            new Date(message.createdAt).getMonth() !== new Date(counter).getMonth() ||
                            new Date(message.createdAt).getFullYear() !== new Date(counter).getFullYear()) {
                            counter = message.createdAt;
                            if (message.userId === undefined) {
                                return ([
                                        <MessageDivider
                                            date={counter}
                                            key={index}
                                        />,
                                        <OwnMessage
                                            onMessageDelete={this.handleMessageDelete}
                                            key={message.id}
                                            message={message}
                                        />
                                ]);
                            } else {
                                return ([
                                        <MessageDivider
                                            date={counter}
                                            key={index}
                                        />,
                                        <Message
                                            key={message.id}
                                            message={message}
                                        />
                                ]);
                            }
                        }
                        counter = message.createdAt;
                        if (message.userId === undefined) {
                            return (
                                <OwnMessage
                                    onMessageDelete={this.handleMessageDelete}
                                    key={message.id}
                                    message={message}
                                />
                            );
                        } else {
                            return (
                                <Message
                                    key={message.id}
                                    message={message}
                                />
                            );
                        }
                    })}
                <MessageInput
                    onMessageEdit={this.handleMessageEdit}
                    addMessage={this.handleMessageAdd}
                />
            </div>
        );
    }
}

export default MessageList;