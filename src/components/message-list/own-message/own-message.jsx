import * as React from 'react';
import EditButton from "../../common/edit-button";
import DeleteButton from "../../common/delete-button";
import {ISOtoTime} from "../../../functions/convert-date";

class OwnMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {message: props.message};
        this.handleEditMessage = this.handleEditMessage.bind(this);
        this.handleDeleteMessage = this.handleDeleteMessage.bind(this);
    }
    handleEditMessage() {
        document.getElementById("hidden-input-id").value = this.state.message.id;
        document.getElementsByClassName("message-input-text")[0].focus();
        document.getElementsByClassName("message-input-text")[0].value = this.state.message.text;
        document.getElementsByClassName("editPostMarker")[0].style.opacity = 1;
    }
    handleDeleteMessage() {
        this.props.onMessageDelete(this.state.message.id);
    }

    render() {
        return(
            <div className={"own-message"}>
                <div className={"message-container"}>
                    <p className={"message-text"}>{this.state.message.text}</p>
                    <span className={"message-time"}>{ISOtoTime(this.state.message.createdAt)}</span>
                    <EditButton
                        onEditMessage={this.handleEditMessage}
                    />
                    <DeleteButton
                        onDeleteMessage={this.handleDeleteMessage}
                    />
                </div>
            </div>
        );
    }
}

export default OwnMessage;