import * as React from 'react';

class Preloader extends React.Component{

    render() {
        return(
            <div className={"preloader"}>
                <div className="lds-roller">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        );
    }
}

export default Preloader;