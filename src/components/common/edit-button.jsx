import * as React from 'react';

class EditButton extends React.Component{
    constructor(props) {
        super(props);
        this.handleEdit = this.handleEdit.bind(this);
    }

    handleEdit() {
        this.props.onEditMessage();
    }

    render() {
        return(
            <svg className={"message-edit"} onClick={e => this.handleEdit()} width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M9.29625 2.38L10.62 3.70313L9.29625 2.38ZM10.1475 1.21438L6.56813 4.79375C6.38318 4.97844 6.25705 5.21374 6.20563 5.47L5.875 7.125L7.53 6.79375C7.78625 6.7425 8.02125 6.61688 8.20625 6.43188L11.7856 2.8525C11.8932 2.74494 11.9785 2.61725 12.0367 2.47671C12.0949 2.33618 12.1249 2.18555 12.1249 2.03344C12.1249 1.88132 12.0949 1.7307 12.0367 1.59016C11.9785 1.44963 11.8932 1.32194 11.7856 1.21438C11.6781 1.10681 11.5504 1.02149 11.4098 0.963281C11.2693 0.905069 11.1187 0.875108 10.9666 0.875108C10.8144 0.875108 10.6638 0.905069 10.5233 0.963281C10.3828 1.02149 10.2551 1.10681 10.1475 1.21438V1.21438Z" stroke="black" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round"/>
                <path d="M10.875 8.375V10.25C10.875 10.5815 10.7433 10.8995 10.5089 11.1339C10.2745 11.3683 9.95652 11.5 9.625 11.5H2.75C2.41848 11.5 2.10054 11.3683 1.86612 11.1339C1.6317 10.8995 1.5 10.5815 1.5 10.25V3.375C1.5 3.04348 1.6317 2.72554 1.86612 2.49112C2.10054 2.2567 2.41848 2.125 2.75 2.125H4.625" stroke="black" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round"/>
            </svg>
        );
    }
}

export default EditButton;