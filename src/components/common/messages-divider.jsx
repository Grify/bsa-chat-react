import * as React from 'react';
import {ISOtoDate} from "../../functions/convert-date";

class MessageDivider extends React.Component{

    render() {
        return(
            <div className={"messages-divider"}>
                <div className="line"></div>
                <div>{ISOtoDate(this.props.date)}</div>
                <div className="line"></div>
            </div>
        );
    }
}

export default MessageDivider;