import * as React from 'react';
import Header from "./header/header";
import MessageList from "./message-list/message-list";
import Preloader from "./common/preloader";

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {loading: true, url: props.url, messages: [{createdAt: "16.07.2020 19:50"}]};
        this.handleChangeMessages = this.handleChangeMessages.bind(this);
    }

    componentDidMount() {
        fetch(this.state.url)
            .then(response => response.json())
            .then(data => {
                this.setState({messages: data, loading: false});
            });
    }

    handleChangeMessages(newMessages) {
        this.setState({messages: newMessages});
    }

    render() {
        return(
            (!this.state.loading) ? (
                <div className={"chat"}>
                    <header>
                        <Header
                            usersCount={new Set(this.state.messages.map(message => message.userId)).size}
                            messagesCount={this.state.messages.length}
                            lastMessageDate={this.state.messages.reduce((mostRecent, item) =>
                                item.createdAt > mostRecent.createdAt
                                    ? item
                                    : mostRecent
                            )}
                        />
                    </header>
                    <main>
                        <MessageList
                            onChangeMessages={this.handleChangeMessages}
                            messages={this.state.messages}
                        />
                    </main>
                </div>
            ) : (
                <div className={"chat"}>
                    <Preloader/>
                </div>
            )
        );
    }
}

export default Chat;