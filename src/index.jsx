import * as React from 'react';
import ReactDOM from 'react-dom';
import Chat from "./components/chat";

ReactDOM.render(
    <Chat
        url={"https://edikdolynskyi.github.io/react_sources/messages.json"}
    />,
    document.getElementById("root")
);